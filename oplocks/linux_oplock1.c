#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define __USE_GNU 1
#include <fcntl.h>
#include <signal.h>
#include <errno.h>

static void sig_io(int sig)
{
	printf("got sigio\n");
}

int main(int argc, const char *argv[])
{
	int fd1;
	int fd2;
	int ret;

	signal(SIGIO, sig_io);
	signal(SIGIO, SIG_IGN);

	errno = 0;
	fd1 = open("tmp1.dat", O_CREAT|O_RDWR);
//	fd1 = open("tmp1.dat", O_CREAT|O_RDONLY);
	printf("fd1[%d]: %s\n", fd1, strerror(errno));

	errno = 0;
	ret = fcntl(fd1, F_SETLEASE, F_WRLCK);
//	ret = fcntl(fd1, F_SETLEASE, F_RDLCK);
	printf("ret[%d]: %s\n", ret, strerror(errno));

	errno = 0;
//	fd2 = open("tmp1.dat", O_CREAT|O_RDWR|O_NONBLOCK);
//	fd2 = open("tmp1.dat", O_CREAT|O_RDONLY|O_NONBLOCK);
//	printf("fd2[%d]: %s\n", fd2, strerror(errno));

	errno = 0;
/*	ret = flock(fd1, LOCK_EX);
	ret = flock(fd1, LOCK_SH);
	printf("ret[%d]: %s\n", ret, strerror(errno));
*/

	errno = 0;
//	ret = fcntl(fd1, F_SETLEASE, F_WRLCK);
//	ret = fcntl(fd1, F_SETLEASE, F_RDLCK);
//	ret = fcntl(fd1, F_SETLEASE, F_UNLCK);
//	printf("ret[%d]: %s\n", ret, strerror(errno));

	errno = 0;
//	fd2 = open("tmp1.dat", O_CREAT|O_RDWR|O_NONBLOCK);
//	printf("fd2[%d]: %s\n", fd2, strerror(errno));

	errno = 0;
//	ret = fcntl(fd2, F_SETLEASE, F_WRLCK);
//	ret = fcntl(fd2, F_SETLEASE, F_RDLCK);
//	printf("ret[%d]: %s\n", ret, strerror(errno));


	sleep(50000);

	return 0;
}
