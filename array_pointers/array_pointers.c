#include <stdio.h>
#include <stdint.h>
int main(void)
{
	char msg[6] = { 'H', 'e', 'l', 'l', 'o', '\0' };
	printf("msg => %p; &msg => %p; &msg[0] => %p\n",
		msg, (char *)&msg, &msg[0]);
	printf("msg => %s; &msg => %s; &msg[0] => %s\n",
		msg, (char *)&msg, &msg[0]);
	return 0;
}
