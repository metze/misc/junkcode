#include <stdio.h>
#include <stdint.h>

static void fn(size_t *v)
{
	printf("sizeof(*v) = %lu\n", sizeof(*v));
}

int main(void)
{
	uint32_t v = 0;
	printf("sizeof(v) = %lu\n", sizeof(v));
	fn((size_t *)&v);
	return 0;
}
