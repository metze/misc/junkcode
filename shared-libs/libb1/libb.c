#include <stdio.h>
#include <stdlib.h>
#include "libb.h"
#include "../liba1/liba.h"

int libbfunc1(int arg)
{
	int ret = 0;
	printf("libb1: libbfunc1(%d) ...\n", arg);
	ret = libafunc1(arg);
	printf("libb1: ... %d\n", ret);
	return ret;
}

