#include <stdio.h>
#include <stdlib.h>
#include "../liba2/liba.h"
#include "../libb1/libb.h"

int main(int argc, const char *argv[])
{
	int ret;

	printf("appa2: ...\n");
	ret = libafunc1(21);
	printf("appa2: ... %d\n", ret);

	printf("appa2: ...\n");
	ret = libafunc2(21);
	printf("appa2: ... %d\n", ret);

	printf("appa2: ...\n");
	ret = libbfunc1(21);
	printf("appa2: ... %d\n", ret);

	return 0;
}

