#include <stdio.h>
#include <stdlib.h>
#include "liba.h"

int _libafunc1(int arg, const char *location)
{
	int ret = -1;
	printf("liba2: _libafunc1(%d, %s) = %d\n", arg, location, ret);
	return ret;
}

int libafunc2(int arg)
{
	int ret = 0;
	printf("liba2: libafunc2(%d) = %d\n", arg, ret);
	return ret;
}

