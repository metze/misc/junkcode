#include <stdio.h>
#include <stdlib.h>
#include "../liba2/liba.h"

#undef libafunc1
int libafunc1(int arg)
{
	int ret = 0;
	int retc;
	printf("liba2-compat1: libafunc1(%d) ...\n", arg);
	retc = _libafunc1(arg, "liba2-compat1-location");
	printf("liba2-compat1: ... %d => %d\n", retc, ret);
	return ret;
}
