# make -C liba1 clean
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/liba1'
rm -f *.o *.so*
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/liba1'
# make -C liba1
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/liba1'
ccache gcc -fPIC   -c -o liba.o liba.c
gcc -shared -o liba.so.1 -Wl,-soname=liba.so.1 liba.o
rm -f liba.so
ln -s liba.so.1 liba.so
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/liba1'
# make -C liba2 clean
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/liba2'
rm -f *.o *.so*
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/liba2'
# make -C liba2
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/liba2'
ccache gcc -fPIC   -c -o liba.o liba.c
gcc -shared -o liba.so.2 -Wl,-soname=liba.so.2 liba.o
rm -f liba.so
ln -s liba.so.2 liba.so
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/liba2'
# make -C liba2-compat1 clean
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/liba2-compat1'
rm -f *.o *.so*
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/liba2-compat1'
# make -C liba2-compat1
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/liba2-compat1'
ccache gcc -fPIC   -c -o liba2-compat1.o liba2-compat1.c
gcc -shared -o liba2-compat1.so -Wl,-soname=liba.so.1 liba2-compat1.o ../liba2/liba.so
rm -f liba.so.1
ln -s liba2-compat1.so liba.so.1
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/liba2-compat1'
# make -C libb1 clean
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/libb1'
rm -f *.o *.so*
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/libb1'
# make -C libb1
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/libb1'
ccache gcc -fPIC   -c -o libb.o libb.c
gcc -shared -o libb.so.1 -Wl,-soname=libb.so.1 libb.o ../liba1/liba.so
rm -f libb.so
ln -s libb.so.1 libb.so
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/libb1'
# make -C appa1 clean
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/appa1'
rm -f *.o *.so* appa1
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/appa1'
# make -C appa1
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/appa1'
ccache gcc    -c -o app.o app.c
gcc -o appa1 app.o ../liba1/liba.so
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/appa1'
# make -C appa2 clean
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/appa2'
rm -f *.o *.so* appa2
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/appa2'
# make -C appa2
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/appa2'
ccache gcc    -c -o app.o app.c
gcc -o appa2 app.o ../liba2/liba.so
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/appa2'
# make -C appa2b1 clean
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/appa2b1'
rm -f *.o *.so* appa2b1
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/appa2b1'
# make -C appa2b1
make: Gehe in Verzeichnis '/tmp/junkcode/shared-libs/appa2b1'
ccache gcc    -c -o app.o app.c
gcc -o appa2b1 app.o ../liba2/liba.so ../libb1/libb.so -Wl,-rpath-link,../liba1/
/usr/bin/ld: warning: liba.so.1, needed by ../libb1/libb.so, may conflict with liba.so.2
make: Verlasse Verzeichnis '/tmp/junkcode/shared-libs/appa2b1'

# LD_LIBRARY_PATH=liba1 appa1/appa1
appa1: ...
liba1: libafunc1(1) = 0
appa1: ... 0

# LD_LIBRARY_PATH=liba1 ldd appa1/appa1
	linux-vdso.so.1 =>  (0x00007fff71ffe000)
	liba.so.1 => liba1/liba.so.1 (0x00007f2669ab8000)
	libc.so.6 => /lib/libc.so.6 (0x00007f2669756000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f2669cb9000)

# LD_LIBRARY_PATH=liba1 appa1/appa1
appa1: ...
liba1: libafunc1(1) = 0
appa1: ... 0

# LD_LIBRARY_PATH=liba2-compat1:liba2 ldd appa1/appa1
	linux-vdso.so.1 =>  (0x00007fff4d9fe000)
	liba.so.1 => liba2-compat1/liba.so.1 (0x00007f8a454a4000)
	libc.so.6 => /lib/libc.so.6 (0x00007f8a45142000)
	liba.so.2 => liba2/liba.so.2 (0x00007f8a44f41000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f8a456a5000)

# LD_LIBRARY_PATH=liba2-compat1:liba2 appa1/appa1
appa1: ...
liba2-compat1: libafunc1(1) ...
liba2: _libafunc1(1, liba2-compat1-location) = -1
liba2-compat1: ... -1 => 0
appa1: ... 0

# LD_LIBRARY_PATH=liba2 ldd appa2/appa2
	linux-vdso.so.1 =>  (0x00007fff009fe000)
	liba.so.2 => liba2/liba.so.2 (0x00007f09f8570000)
	libc.so.6 => /lib/libc.so.6 (0x00007f09f820e000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f09f8771000)

# LD_LIBRARY_PATH=liba2 appa2/appa2
appa2: ...
liba2: _libafunc1(2, app.c:10) = -1
appa2: ... -1
appa2: ...
liba2: libafunc2(2) = 0
appa2: ... 0

# LD_LIBRARY_PATH=liba2:libb1:liba2-compat1 ldd appa2b1/appa2b1
	linux-vdso.so.1 =>  (0x00007fff725fe000)
	liba.so.2 => liba2/liba.so.2 (0x00007fa46a096000)
	libb.so.1 => libb1/libb.so.1 (0x00007fa469e95000)
	libc.so.6 => /lib/libc.so.6 (0x00007fa469b33000)
	liba.so.1 => liba2-compat1/liba.so.1 (0x00007fa469932000)
	/lib64/ld-linux-x86-64.so.2 (0x00007fa46a297000)

# LD_LIBRARY_PATH=liba2:libb1:liba2-compat1 appa2b1/appa2b1
appa2: ...
liba2: _libafunc1(21, app.c:11) = -1
appa2: ... -1
appa2: ...
liba2: libafunc2(21) = 0
appa2: ... 0
appa2: ...
libb1: libbfunc1(21) ...
liba2-compat1: libafunc1(21) ...
liba2: _libafunc1(21, liba2-compat1-location) = -1
liba2-compat1: ... -1 => 0
libb1: ... 0
appa2: ... 0
