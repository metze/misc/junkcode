#include <stdint.h>
#include <stdio.h>
#include <assert.h>

int main(void)
{
	int32_t v;
	uint32_t u32;

	v = 0xe2340001;

	u32 = (v >> 16);

	printf(" v[0x%08X] u32[%08X]\n", v, u32);
	assert(u32 == 0xFFFFe234);

	v = 0x12340001;

	u32 = (v >> 16);

	printf(" v[0x%08X] u32[%08X]\n", v, u32);
	assert(u32 == 0x00001234);

	return 0;
}
