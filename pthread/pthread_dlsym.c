#include <stdlib.h>
#include <dlfcn.h>

//int pthread_mutex_init(void);
int pthread_create(void);

int main(void)
{
	void *h;
	void *f;

	h = dlopen(NULL,RTLD_NOW|RTLD_GLOBAL);
	if (!h) return 255;

	f = dlsym(h, "pthread_mutex_init");

	if (!f) return 127;

//pthread_mutex_init();
pthread_create();

	return 0;
}
