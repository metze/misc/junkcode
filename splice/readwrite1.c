#define _GNU_SOURCE
#include <unistd.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/sendfile.h>

int main(int argc, const char * const *argv)
{
	ssize_t ret;
	int pfd[2];
	int rc;
	void *ptr;
	struct iovec iov[2];

	static const uint8_t buf[0x2000];

	int zfd = open("/dev/zero", O_RDWR);
	loff_t zofs = 5;
	size_t zlen = 0x20000;
	int nfd = open("/dev/null", O_WRONLY);
	loff_t nofs = 5;
	size_t nlen = 0x200;
	int i;
	int sfd[2];
	rc = socketpair(PF_UNIX, SOCK_STREAM, 0, sfd);

	rc = pipe(pfd);

#if 0
	errno = 0;
	ret = write(pfd[1], buf, sizeof(buf));
	printf("%d: ret[%lld] errno[%d/%s]\n", __LINE__,
		(long long int)ret, errno, strerror(errno));
#endif

for (i=0; i < 0x10000; i++) {
	off_t ofs = 0;

	errno = 0;
	ret = sendfile(sfd[1], zfd, NULL/*&ofs*/, nlen);
	if (ret < 0) {
		printf("%d: ret[%lld] errno[%d/%s]\n", __LINE__,
			(long long int)ret, errno, strerror(errno));
		break;
	}
}
#if 0
	errno = 0;
	ret = splice(pfd[0], NULL, nfd, &nofs, nlen, SPLICE_F_MOVE|SPLICE_F_NONBLOCK);
	printf("%d: ret[%lld] errno[%d/%s]\n", __LINE__,
		(long long int)ret, errno, strerror(errno));
#endif

	return 0;
}
