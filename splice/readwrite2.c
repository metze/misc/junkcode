#define _GNU_SOURCE
#include <unistd.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/sendfile.h>

int main(int argc, const char * const *argv)
{
	int zfd = open("/dev/zero", O_RDONLY);
	int nfd = open("/dev/null", O_WRONLY);
	size_t total = 0;
#define TOTAL 0xFFFFFFFFF
#define BUFFER 0x100000
	size_t i = 0;

	while (total < TOTAL) {
		uint8_t buf[BUFFER];
		ssize_t zret;
		ssize_t nret;

		errno = 0;
		zret = read(zfd, buf, sizeof(buf));
		if (zret < 0) {
			printf("%d: zret[%lld] errno[%d/%s]\n", __LINE__,
				(long long int)zret, errno, strerror(errno));
			break;
		}

		errno = 0;
		nret = write(nfd, buf, zret);
		if (nret < 0) {
			printf("%d: nret[%lld] errno[%d/%s]\n", __LINE__,
				(long long int)nret, errno, strerror(errno));
			break;
		}
		if (nret != zret) {
			printf("%d: zret[%lld] nret[%lld]\n", __LINE__,
				(long long int)zret,
				(long long int)nret);
			break;
		}

		total += nret;
		i++;
	}

	printf("i[%lld]\n", (long long int)i);
	return 0;
}
