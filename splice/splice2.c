#define _GNU_SOURCE
#include <unistd.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/sendfile.h>

int main(int argc, const char * const *argv)
{
	int pfd[2];
	int zfd;
	int nfd;
	size_t i;
	size_t total_size;
	size_t current_size = 0;
	uint8_t *buffer = NULL;;
	size_t buffer_size;

	if (argc != 4) {
		printf("%s: usage %s <'splice'|'rw'> <total_size> <buffer_size>\n",
			argv[0], argv[0]);
		exit(1);
	}

	total_size = strtoull(argv[2], NULL, 0);
	buffer_size = strtoull(argv[3], NULL, 0);

	if (strcmp(argv[1], "splice") == 0) {
		pipe(pfd);
		fcntl(pfd[1], F_SETPIPE_SZ, buffer_size);
		buffer_size = fcntl(pfd[1], F_GETPIPE_SZ);
	} else {
		buffer = malloc(buffer_size);
	}

	zfd = open("/dev/zero", O_RDONLY);
	nfd = open("/dev/null", O_WRONLY);

	while (current_size < total_size) {
		ssize_t zret;
		ssize_t nret;

		errno = 0;
		if (buffer) {
			zret = read(zfd, buffer, buffer_size);
		} else {
			zret = splice(zfd, NULL, pfd[1], NULL,
				      buffer_size, SPLICE_F_MOVE|SPLICE_F_NONBLOCK);
		}
		if (zret < 0) {
			printf("%d: zret[%lld] errno[%d/%s]\n", __LINE__,
				(long long int)zret, errno, strerror(errno));
			break;
		}

		errno = 0;
		if (buffer) {
			nret = write(nfd, buffer, zret);
		} else {
			nret = splice(pfd[0], NULL, nfd, NULL,
				      zret, SPLICE_F_MOVE|SPLICE_F_NONBLOCK);
		}
		if (nret < 0) {
			printf("%d: nret[%lld] errno[%d/%s]\n", __LINE__,
				(long long int)nret, errno, strerror(errno));
			break;
		}
		if (nret != zret) {
			printf("%d: zret[%lld] nret[%lld]\n", __LINE__,
				(long long int)zret,
				(long long int)nret);
			break;
		}

		current_size += nret;
		i++;
	}

	printf("%s: i[%lld] total_size[%lld] buffer_size[%lld => %lld]\n",
		argv[1],
		(long long int)i,
		(long long int)total_size,
		(long long int)buffer_size,
		(long long int)(total_size/i));
	return 0;
}
