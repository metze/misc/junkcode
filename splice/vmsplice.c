#define _GNU_SOURCE
#include <unistd.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

int main(int argc, const char * const *argv)
{
	ssize_t ret;
	int fd[2];
	int rc;
	void *ptr;
	struct iovec iov[2];

	iov[0].iov_len = 64*1024;
	iov[0].iov_base = malloc(iov[0].iov_len);
	iov[1].iov_len = 0xFF;
	iov[1].iov_base = malloc(iov[1].iov_len);

	rc = pipe(fd);

	ret = vmsplice(fd[1], iov, 2, 0);
	printf("%d: ret[%lld] errno[%d/%s]\n", __LINE__,
		(long long int)ret, errno, strerror(errno));

	return 0;
}
