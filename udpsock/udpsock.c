/*
   Copyright (C) Ronnie Sahlberg  2007
   Copyright (C) Andrew Tridgell  2007
   Copyright (C) Stefan Metzmacher 2010

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

/*
 * this is largely copied from the ctdb sources...
 * compile it with:
 *
 *    gcc -g -o udpsock udpsock.c -Wall -Wfatal-errors
 */

#define BSD_COMP 1
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef char bool;
#define true 1
#define false 0

#include <string.h>
#include <errno.h>

#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/tcp.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <net/if.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include <netinet/if_ether.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <net/if_arp.h>

union generic_sockaddr {
	struct sockaddr sa;
	struct sockaddr_in sin;
	struct sockaddr_in6 sin6;
	struct sockaddr_storage ss;
};

static bool parse_ipv4(const char *s, unsigned port, struct sockaddr_in *sin)
{
	sin->sin_family = AF_INET;
	sin->sin_port   = htons(port);

	if (inet_pton(AF_INET, s, &sin->sin_addr) != 1) {
		printf("inet_pton failed to translate %s into sin_addr\n", s);
		return false;
	}

	return true;
}

static bool parse_ipv6(const char *s, const char *ifaces, unsigned port, struct sockaddr_in6 *sin6)
{
	sin6->sin6_family   = AF_INET6;
	sin6->sin6_port     = htons(port);
	sin6->sin6_flowinfo = 0;
	sin6->sin6_scope_id = 0;

	if (inet_pton(AF_INET6, s, &sin6->sin6_addr) != 1) {
		printf("inet_pton failed to translate %s into sin6_addr\n", s);
		return false;
	}

	if (ifaces && IN6_IS_ADDR_LINKLOCAL(&sin6->sin6_addr)) {
		if (strchr(ifaces, ',')) {
			printf("Link local address %s "
				"is specified for multiple ifaces %s\n",
				s, ifaces);
			return false;
		}
		sin6->sin6_scope_id = if_nametoindex(ifaces);
	}

	return true;
}

/*
  parse an ip
 */
static bool parse_ip(const char *addr, const char *ifaces, unsigned port, union generic_sockaddr *saddr)
{
	char *p;
	bool ret;

	/* now is this a ipv4 or ipv6 address ?*/
	p = strchr(addr, ':');
	if (p == NULL) {
		ret = parse_ipv4(addr, port, &saddr->sin);
	} else {
		ret = parse_ipv6(addr, ifaces, port, &saddr->sin6);
	}

	return ret;
}

/*
  parse a ip:port pair
 */
static bool parse_ip_port(const char *addr, const char *port_str, union generic_sockaddr *saddr)
{
	unsigned port;
	char *endp = NULL;
	bool ret;

	port = strtoul(port_str, &endp, 10);
	if (endp == NULL || *endp != 0) {
		printf("Trailing garbage after the port in %s\n", port_str);
		return false;
	}

	/* now is this a ipv4 or ipv6 address ?*/
	ret = parse_ip(addr, NULL, port, saddr);

	return ret;
}

static bool parse_bool(const char *str)
{
	unsigned val;
	char *endp = NULL;

	val = strtoul(str, &endp, 10);
	if (endp == NULL || *endp != 0) {
		printf("Trailing garbage after the port in %s\n", str);
		return false;
	}

	if (val != 0) {
		return true;
	}

	return false;
}

struct udpsock {
#define UDPSOCK_MAGIC 0x5544504D
	uint32_t magic;
	uint32_t idx;
	uint32_t echo_idx;
	uint32_t seqnum;
	char str[128];
	const char *local_ipv4_str;
	const char *local_port_str;
	const char *bcast_str;
	const char *reuseaddr_str;
	const char *echo_str;
	const char *remote_ipv4_str;
	const char *remote_port_str;

	union generic_sockaddr local_sockaddr;
	union generic_sockaddr _remote_sockaddr;
	union generic_sockaddr *remote_sockaddr;
	bool reuseaddr;
	bool bcast;
	bool echo;
	int fd;
};

static void print_udpsock(const struct udpsock *s)
{
	printf("[%d] local[%s:%s] bcast[%d] reuse[%d] echo[%d] remote[%s:%s]\n",
		s->idx,
		s->local_ipv4_str, s->local_port_str,
		s->bcast, s->reuseaddr, s->echo,
		s->remote_ipv4_str, s->remote_port_str);
}

static bool parse_socket(const char *str, struct udpsock *s, int idx)
{
	char *p;
	bool ok;

	/*
	 * for now ipv4 only
	 *
	 * <local-ipv4>:<local-port>:<bcast:1:0>:<reuseaddr:1:0>:<echo:1:0>:<dstipv4>:<destport>
	 */

	memset(s, 0, sizeof(*s));
	s->magic = UDPSOCK_MAGIC;
	s->idx = idx;
	s->seqnum = 1;
	sprintf(s->str, "%d:%s", idx, str);
	p = s->str;

	p = strchr(p, ':');
	if (p == NULL) {
		printf("LINE:%u: invalid param[%s]\n", __LINE__, str);
		return false;
	}
	p[0] = '\0';
	p++;

	s->local_ipv4_str = p;

	p = strchr(p, ':');
	if (p == NULL) {
		printf("LINE:%u: invalid param[%s]\n", __LINE__, str);
		return false;
	}
	p[0] = '\0';
	p++;

	s->local_port_str = p;

	p = strchr(p, ':');
	if (p == NULL) {
		printf("LINE:%u: invalid param[%s]\n", __LINE__, str);
		return false;
	}
	p[0] = '\0';
	p++;

	s->bcast_str = p;

	p = strchr(p, ':');
	if (p == NULL) {
		printf("LINE:%u: invalid param[%s]\n", __LINE__, str);
		return false;
	}
	p[0] = '\0';
	p++;

	s->reuseaddr_str = p;

	p = strchr(p, ':');
	if (p == NULL) {
		printf("LINE:%u: invalid param[%s]\n", __LINE__, str);
		return false;
	}
	p[0] = '\0';
	p++;

	s->echo_str = p;

	p = strchr(p, ':');
	if (p == NULL) {
		s->remote_ipv4_str = &s->str[sizeof(s->str)-1];
		s->remote_port_str = &s->str[sizeof(s->str)-1];
	} else {
		p[0] = '\0';
		p++;

		s->remote_ipv4_str = p;

		p = strchr(p, ':');
		if (p == NULL) {
			printf("LINE:%u: invalid param[%s]\n", __LINE__, str);
			return false;
		}
		p[0] = '\0';
		p++;

		s->remote_port_str = p;
	}

	ok = parse_ip_port(s->local_ipv4_str, s->local_port_str, &s->local_sockaddr);
	if (!ok) {
		printf("invalid local ip\n");
		goto fail;
	}

	s->bcast = parse_bool(s->bcast_str);
	s->reuseaddr = parse_bool(s->reuseaddr_str);
	s->echo = parse_bool(s->echo_str);

	if (strlen(s->remote_ipv4_str) > 0) {
		ok = parse_ip_port(s->remote_ipv4_str, s->remote_port_str, &s->_remote_sockaddr);
		if (!ok) {
			printf("invalid remote ip\n");
			goto fail;
		}
		s->remote_sockaddr = &s->_remote_sockaddr;
	}

	return true;
fail:
	print_udpsock(s);
	return false;
}

static bool setup_udpsock(struct udpsock *s)
{
	int ret;
	int val;
	int sa_socklen;

	s->fd = socket(s->local_sockaddr.sa.sa_family, SOCK_DGRAM, 0);
	if (s->fd == -1) {
		printf("[%d] failed to call socket(%d, %d, %d) - %s\n",
			s->idx,
			s->local_sockaddr.sa.sa_family, SOCK_DGRAM, 0,
			strerror(errno));
		return false;
	}

	if (s->reuseaddr) {
		val = 1;
		ret = setsockopt(s->fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
		if (ret == -1) {
			printf("[%d] failed to call setsockopt(SO_REUSEADDR) - %s\n",
				s->idx, strerror(errno));
			return false;
		}
	}

	switch (s->local_sockaddr.sa.sa_family) {
	case AF_INET:
		sa_socklen = sizeof(s->local_sockaddr.sin);
		break;
	case AF_INET6:
		sa_socklen = sizeof(s->local_sockaddr.sin6);
		break;
	}

	ret = bind(s->fd, &s->local_sockaddr.sa, sa_socklen);
	if (ret == -1) {
		printf("[%d] failed to call bind(%s:%s) - %s\n",
			s->idx,
			s->local_ipv4_str, s->local_port_str,
			strerror(errno));
		return false;
	}

	if (s->bcast) {
		val = 1;
		ret = setsockopt(s->fd, SOL_SOCKET, SO_BROADCAST, &val, sizeof(val));
		if (ret == -1) {
			printf("[%d] failed to call setsockopt(SO_REUSEADDR) - %s\n",
				s->idx, strerror(errno));
			return false;
		}
	}

	return true;
}

static bool sendto_udpsock(struct udpsock *s)
{
	ssize_t ret;
	int sa_socklen;

	if (!s->remote_sockaddr) {
#if 0
		printf("[%d] skip sendto - no remote sockaddr\n",
			s->idx);
#endif
		return true;
	}

	s->seqnum++;

	switch (s->remote_sockaddr->sa.sa_family) {
	case AF_INET:
		sa_socklen = sizeof(s->remote_sockaddr->sin);
		break;
	case AF_INET6:
		sa_socklen = sizeof(s->remote_sockaddr->sin6);
		break;
	}
	ret = sendto(s->fd,
		     s, sizeof(*s), 0,
		     &s->remote_sockaddr->sa,
		     sa_socklen);
	if (ret == -1) {
		printf("[%d] sendto(%s:%s => %s:%s - failed - %s\n",
			s->idx, s->local_ipv4_str, s->local_port_str,
			s->remote_ipv4_str, s->remote_port_str,
			strerror(errno));
		return false;
	}

	printf("[%d] sendto(%s:%s => %s:%s - ok\n",
		s->idx, s->local_ipv4_str, s->local_port_str,
		s->remote_ipv4_str, s->remote_port_str);

	return true;
}

static ssize_t tsocket_bsd_pending(int fd)
{
	int ret, error;
	int value = 0;
	socklen_t len;

	ret = ioctl(fd, FIONREAD, &value);
	if (ret == -1) {
		return ret;
	}

	if (ret != 0) {
		/* this should not be reached */
		errno = EIO;
		return -1;
	}

	if (value != 0) {
		return value;
	}

	error = 0;
	len = sizeof(error);

	/*
	 * if no data is available check if the socket is in error state. For
	 * dgram sockets it's the way to return ICMP error messages of
	 * connected sockets to the caller.
	 */
	ret = getsockopt(fd, SOL_SOCKET, SO_ERROR, &error, &len);
	if (ret == -1) {
		return ret;
	}
	if (error != 0) {
		errno = error;
		return -1;
	}
	return 0;
}

static bool recvfrom_udpsock(struct udpsock *s)
{
	ssize_t pending;
	ssize_t ret;
	uint8_t buf[1024];
	union generic_sockaddr src;
	socklen_t src_len;
	char src_str[256];
	uint32_t r_seq;
loop:
	memset(src_str, 0, sizeof(src_str));
	src_len = sizeof(src.ss);
	r_seq = 0;
	pending = tsocket_bsd_pending(s->fd);
	if (pending == 0) {
		return true;
	}
	if (pending == -1) {
		printf("[%d] pending returned - %s\n",
			s->idx, strerror(errno));
		return false;
	}

	ret = recvfrom(s->fd, buf, pending, 0,
		       &src.sa, &src_len);
	if (ret == -1) {
		printf("[%d] recvfrom(%s:%s) returned - %s\n",
			s->idx, s->local_ipv4_str, s->local_port_str,
			strerror(errno));
		return false;
	}

	if (ret == sizeof(*s)) {
		struct udpsock *p = (struct udpsock *)buf;
		if (p->magic == UDPSOCK_MAGIC) {
			p->echo_idx = s->idx;
			r_seq = p->seqnum;
		}
	}

	printf("[%d] received %d bytes (rseq[%d]) on %s:%s from %s:%d\n",
		s->idx, (int)ret, (int)r_seq,
		s->local_ipv4_str, s->local_port_str,
		inet_ntop(src.sa.sa_family, &src.sin.sin_addr, src_str, sizeof(src.ss)),
		ntohs(src.sin.sin_port));

	if (!s->echo) {
		goto loop;
	}

	ret = sendto(s->fd, buf, ret, 0,
		     &src.sa, src_len);
	if (ret == -1) {
		printf("[%d] sendto echo (r_seq[%d]) of %d bytes on %s:%s to %s:%d failed - %s\n",
			s->idx, (int)r_seq, (int)ret,
			s->local_ipv4_str, s->local_port_str,
			inet_ntop(src.sa.sa_family, &src.sa, src_str, sizeof(src.ss)),
			ntohs(src.sin.sin_port), strerror(errno));
		return false;
	}

	printf("[%d] echoed (r_seq[%d]) %d bytes on %s:%s to %s:%d\n",
		s->idx, (int)r_seq, (int)ret,
		s->local_ipv4_str, s->local_port_str,
		inet_ntop(src.sa.sa_family, &src.sin.sin_addr, src_str, sizeof(src.ss)),
		ntohs(src.sin.sin_port));

	return true;
}

int main(int argc, const char *argv[])
{
	int i;
#define UDPSOCK_COUNT 10
	struct udpsock sock[UDPSOCK_COUNT];
	bool ok;
	int count;

	if (argc < 2) {
		printf("usage: %s <config> [<config>] ...\n", argv[0]);
		printf("config := <localipv4>:<localport>:<bcast:1:0>:"
		       "<reuseaddr:1:0>:<echo:1:0>[:<dstipv4>:<destport>]\n");
		return -1;
	}

	for (i=0; i < (argc - 1) && i < UDPSOCK_COUNT; i++) {

		ok = parse_socket(argv[i+1], &sock[i], i);
		if (!ok) {
			printf("Bad argument '%s'\n", argv[i+1]);
			return -1;
		}
	}
	count = i;

	for (i=0; i < count; i++) {
		print_udpsock(&sock[i]);

		ok = setup_udpsock(&sock[i]);
		if (!ok) {
			printf("failed to setup_udpsock '%s'\n", argv[i+1]);
			return -1;
		}
	}

loop:
	for (i=0; i < count; i++) {
		ok = recvfrom_udpsock(&sock[i]);
		if (!ok) {
			printf("failed to recvfrom_udpsock() '%s'\n", argv[i+1]);
			return -1;
		}
		ok = sendto_udpsock(&sock[i]);
		if (!ok) {
			printf("failed to sendto_udpsock() '%s'\n", argv[i+1]);
			return -1;
		}
		sleep(1);
	}

	goto loop;

	return -1;
}

