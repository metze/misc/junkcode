/*
 * Author: Stefan Metzmacher <metze@samba.org>
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/*
 * start this with gdb and break on 'mybreak'
 * and search for [heap] in /proc/<pid>/smaps
 * at each step.
 */

static void mybreak(void)
{
}

int main(void)
{
	int i;
	void *p1[10000];
	void *p2[10000];

	printf("pid: %d\n", getpid());

	for (i=0; i < 10000; i++) {
		p1[i] = malloc(10000);
		memset(p1[i], 0xDF, 10000);
		p2[i] = malloc(10);
		memset(p2[i], 0xAB, 10);
	}

	mybreak();

	for (i=0; i < 10000; i++) {
		free(p1[i]);
	}

	mybreak();

	for (i=0; i < 10000; i++) {
		p1[i] = malloc(10000);
		memset(p1[i], 0xDF, 10000);
	}

	mybreak();

	for (i=0; i < 10000; i++) {
		free(p1[i]);
	}

	mybreak();

	for (i=0; i < 10000; i++) {
		free(p2[i]);
	}

	mybreak();

	return 0;
}
